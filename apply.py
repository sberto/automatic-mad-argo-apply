from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import os # to get the resume file
import time # to sleep

DEBUG = True
retry = False

driver = webdriver.Firefox(executable_path='./geckodriver.exe')

# sample application links if we don't want to run get_links.py
URL_MAD = 'https://mad.portaleargo.it/#/trasmetti'

thisFolder = os.getcwd() + '/'
filesFolder = thisFolder + 'files/'

# Fill in this dictionary with your personal details!
MAD_APP = {
    "Nome": "Dodo",
    "Cognome": "Dodo",
    "Codice fiscale": "DODOPP81E35D269A",
    "Indirizzo": "Via dei Dodo",
    "Comune": "Genova",
    "Email": "dodomail@gmail.com",
    "Pec": "",
    "Telefono": "345365DODO",
    "Documento": "CARTA_ID.pdf",
    "Curriculum": "CV.pdf",
    "Domanda": "MAD.pdf",
}

MAD_APP2 = {
    "Livello di istruzione": "Laurea di magistrale", # Laurea di primo livello   OR     Laurea magistrale
    "Titolo di studio": "Laurea in Fisica",
    "Punteggio": "999/110"
}

ordine = 'Superiore II grado' # Elementare, Secondaria I grado, Comprensivo, Superiore II grado, Omnicomprensivo

if DEBUG:
    classi = ['A020']
else:
    classi = ['A020','A026','A027','A033','A040','A041','A043','A047']

provinces = ['Genova']
indexProv = 0
indexSchool = 0
schools = []

def yesno(string):
    x = ""
    print(string)
    while(x.lower() not in ["yes","y","no","n"]):
        x = input()
    if x.lower() in ["yes","y"]:
        return True
    return False

def click(searchStr):
    try:
        element = driver.find_element_by_xpath(searchStr)
    except:
        print("Could not find "+searchStr+". Trying again...")
        time.sleep(1)
        try:
            element = driver.find_element_by_xpath(searchStr)
        except:
            print("NOT FOUND")
            raise
    try:
        element.click()
    except:
        print("Could not fill "+searchStr+". Trying again...")
        time.sleep(1)
        try:
            element.click()
        except:
            print("NOT CLICKED")
            raise

def fill(searchStr, text):
    try:
        element = driver.find_element_by_xpath(searchStr)
    except:
        print("Could not find "+searchStr+". Trying again...")
        time.sleep(1)
        try:
            element = driver.find_element_by_xpath(searchStr)
        except:
            print("NOT FOUND")
            raise

    try:
        element.send_keys(text)
    except:
        print("Could not fill "+searchStr+". Trying again...")
        time.sleep(1)
        try:
            element.send_keys(text)
        except:
            print("NOT FILLED")
            raise

def fillForm():
    waitFor = "//*[contains(text(),'Nome')]"
    WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.XPATH, waitFor)))
    time.sleep(3)
    
    for textExpected in MAD_APP:
        submitText = MAD_APP[textExpected]

        if textExpected in ["Documento", "Curriculum", "Domanda"]:
            searchStr = "//label[contains(text(),'"+ textExpected +"')]/parent::div"
            click(searchStr)
            waitFor = "//*[contains(text(),'"+submitText+"')]"
            print("Please load "+submitText)
            WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.XPATH, waitFor)))
        else:
            searchStr = "//label[contains(text(),'"+ textExpected +"')]/parent::div/input"
            fill(searchStr, submitText)
            if textExpected=='Comune':
                searchStr = "//*[contains(text(),'" + submitText.upper() + "')]/parent::div"
                time.sleep(0.2)
                click(searchStr)

def fillForm2():
    waitFor = "//*[contains(text(),'Livello')]"
    WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.XPATH, waitFor)))
    time.sleep(3)

    for textExpected in MAD_APP2:
        submitText = MAD_APP2[textExpected]
        if textExpected == "Livello di istruzione":
            try:
                click("//label[contains(text(),'" + textExpected + "')]/parent::div")
            except:
                driver.find_element_by_id('input-146').click()

            try:
                time.sleep(0.5)
                click("//*[contains(text(),'" + submitText + "')]/parent::div")
            except:
                if ('triennale' in submitText) or ('primo' in submitText):
                    driver.find_element_by_id('list-item-741-2').click()
                else:
                    driver.find_element_by_id('list-item-741-3').click()
        else:
            searchStr = "//label[contains(text(),'"+ textExpected +"')]/parent::div/input"
            try:
                inputBox = driver.find_element_by_xpath(searchStr)
                inputBox.send_keys(submitText)
            except:
                print('Error: ' + textExpected + ': ' + submitText)

def avanti():
    buttons = driver.find_elements_by_css_selector('.v-btn__content')
    for button in buttons:
        if button.text == "AVANTI":
            try:
                button.click()
                break
            except:
                print('AVANTI Error')

if __name__ == '__main__':
    driver.implicitly_wait(1) # seconds
    driver.get(URL_MAD)
    while(True):
        try:
            # Page 1
            WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.row:nth-child(3) .v-chip:nth-child(1) > .v-chip__content')))
            driver.find_element_by_css_selector('.row:nth-child(3) .v-chip:nth-child(1) > .v-chip__content').click() # DA MODIFICARE
            click("//span[contains(text(), '"+ordine+"')]/parent::span")
            
            WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.row:nth-child(7)')))
            row = driver.find_element_by_css_selector('.row:nth-child(7)')
            row.click()
            textfield = driver.find_element_by_id('input-53')
            for classe in classi:
                textfield.send_keys(classe)
                click("//*[contains(text(), '"+classe+"')]")
                if classe != classi[-1]:
                    for ch in classe:
                        textfield.send_keys(Keys.BACK_SPACE)
            
            avanti()

            # Page 2
            WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.col-sm-6')))
            driver.find_element_by_css_selector('.col-sm-6').click()
            time.sleep(0.5)
            click("//div[text()='GRATUITO']")
            
            if (len(schools)!=0) and (indexSchool == len(schools)):
                if (len(provinces)!=0) and (indexProv == len(provinces)):
                    pass
                indexProv+=1
                indexSchool=0
            province = provinces[indexProv]
            fill("//input[@placeholder='Cerca scuole per Regione, Provincia...']",province)
            click("//span[contains(text(),'"+province+"')]/parent::div")
            
            WebDriverWait(driver, 20).until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.v-list-item--link')))

            try:
                page_source = driver.page_source
                soup = BeautifulSoup(page_source, features="html.parser")
                buttons = soup.find_all(text=lambda x: province in x)
                all_buttons = soup.find_all("div", {"class": "v-list-item__subtitle"})
            except:
                print("Soup error.")
                raise
            
            try:
                schools = [i for i,x in enumerate(all_buttons) if x.text in buttons]
                school = schools[indexSchool]
            except:
                print("School setup error.")
                raise

            try:
                to_click = '.v-list-item--link:nth-child('+str(2*school+1)+')'
                element = driver.find_element_by_css_selector(to_click)
            except:
                print('School element not selected.')
                raise

            try:
                please_scroll = True
                for i in range(50):
                    try:
                        element.click()
                        break
                    except:
                        if please_scroll:
                            print('Please scroll to '+ str(all_buttons[school].text))
                            please_scroll = False
                        time.sleep(0.1)
            except:
                print('Error with ' + str(all_buttons[school].text))
            
            avanti()

            # Page 3
            fillForm()
            avanti()
            
            # Page 4
            fillForm2()
            avanti()
            
            # Page 5
            if not DEBUG:
                # click("//span[contains(text(),'Trasmetti')]")
                click("//span[contains(text(),'Trasmetti')]/parent::button")
            print("Apply: " + str(indexSchool+1) + "/" + str(len(schools)) +" for " + province)
            time.sleep(10)
            
            indexSchool+=1
            driver.refresh()
        except:
            print(f"Failed " + str(indexSchool+1) + "/" + str(len(schools)) +" for " + province)
            
            
            if not yesno("Do you want to continue?"):
                driver.close()
                exit()


            time.sleep(100)
            
            if not retry:
                print("Retrying")
                retry = True
            else:
                indexSchool+=1
            
            driver.refresh()
            
    
    driver.close()
