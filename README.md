# ARGO Automatic 0.1
Send your MAD to mad.portaleargo.it with this automatic script.

## Installation

You have to install [Python](https://www.python.org/), [Selenium](https://www.selenium.dev/) and [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).

```bash
pip install selenium, beautifulsoup4
```

I will make it ready to be installed with [pip](https://pip.pypa.io/en/stable/) soon.

## Usage

Put your data in the file. Please bear in mind that during the apply the program will wait to a specific name when loading the files.

Due to issues with javascript the script requires to **manually** input the files. Sorry.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
